# Cidenet Talent - Frontend

Este proyecto corresponde al frontend de la aplicación de registro de empleados de Cidenet S.A.S., desarrollado con Angular.

## Instrucciones de Instalación y Ejecución

### Clonar el Repositorio

git clone https://gitlab.com/Alejo98/cidenet-talent-ui.git cidenet-talent-ui
cd cidenet-talent-ui

### Instalar Dependencias

npm install

### Ejecutar la Aplicación
ng serve

La aplicación estará disponible en http://localhost:4200/.

### Funcionalidades Implementadas

Registro de empleados: Permite registrar nuevos empleados con la información requerida.
Consulta de empleados: Lista todos los empleados y permite filtrarlos por diversos criterios.
Edición de empleados: Permite editar la información de un empleado existente.

### Estructura del Proyecto

El código fuente del frontend se encuentra en el directorio cidenet-talent-ui. Para obtener más detalles sobre la implementación y la estructura del código, consulte la documentación interna en el directorio correspondiente.