import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Empleado } from './shared/models/empleado';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {
  private apiUrl = 'http://localhost:5000/api/empleados/'; // Actualiza con la URL de tu backend

  constructor(private http: HttpClient) { }

  registrarEmpleado(nuevoEmpleado: Empleado): Observable<any> {
    return this.http.post(`${this.apiUrl}registrar`, nuevoEmpleado);
  }

  filtrarEmpleados(filtroNombre: string): Observable<Empleado[]> {
    return this.http.get<Empleado[]>(`${this.apiUrl}?filtroNombre=${filtroNombre}`);
  }

  eliminarEmpleado(empleadoId: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}${empleadoId}`);
  }
  consultarEmpleado(empleadoId: number): Observable<Empleado> {
    return this.http.get<Empleado>(`${this.apiUrl}${empleadoId}`);
  }
  obtenerEmpleadoPorId(empleadoId: any): Observable<Empleado> {
    return this.http.get<Empleado>(`${this.apiUrl}${empleadoId}`);
  }
  consultarEmpleados(): Observable<Empleado[]> {
    return this.http.get<Empleado[]>(this.apiUrl);
  }
  guardarCambios(empleado: Empleado): Observable<any> {
    return this.http.put(`${this.apiUrl}${empleado.id}`, empleado);
  }
}
