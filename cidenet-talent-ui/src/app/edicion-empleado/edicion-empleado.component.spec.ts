import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdicionEmpleadoComponent } from './edicion-empleado.component';

describe('EdicionEmpleadoComponent', () => {
  let component: EdicionEmpleadoComponent;
  let fixture: ComponentFixture<EdicionEmpleadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdicionEmpleadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdicionEmpleadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
