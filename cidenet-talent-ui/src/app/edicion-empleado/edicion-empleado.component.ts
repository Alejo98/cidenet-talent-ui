// edicion-empleado.component.ts

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmpleadoService } from '../empleado.service';
import { Empleado } from '../shared/models/empleado';

@Component({
  selector: 'app-edicion-empleado',
  templateUrl: './edicion-empleado.component.html',
  styleUrls: ['./edicion-empleado.component.sass']
})
export class EdicionEmpleadoComponent implements OnInit {
  empleadoEditar: Empleado = {} as Empleado; // Usar {} as Empleado para inicializar como objeto vacío

  constructor(private route: ActivatedRoute, private empleadoService: EmpleadoService, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const empleadoId = params.get('id');
      if (empleadoId) {
        // Lógica para obtener detalles del empleado por su ID y asignar a empleadoEditar
        this.empleadoService.obtenerEmpleadoPorId(empleadoId).subscribe(
          empleado => {
            this.empleadoEditar = empleado;
          },
          error => {
            console.error('Error al obtener detalles del empleado:', error);
          }
        );
      }
    });    
  }

  editarEmpleado() {
    console.log(this.empleadoEditar);
    this.empleadoService.guardarCambios(this.empleadoEditar).subscribe(
      response => {
        console.log('Cambios guardados exitosamente:', response);
        // Navegar de nuevo a la pantalla de consulta de empleados
        this.router.navigate(['/consulta-empleados']);
      },
      error => {
        console.error('Error al guardar cambios:', error);
      }
    );
  }
}
