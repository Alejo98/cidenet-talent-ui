import { Component } from '@angular/core';
import { EmpleadoService } from '../empleado.service';
import { Empleado } from '../shared/models/empleado';
import { formatDate } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-registro-empleado',
  templateUrl: './registro-empleado.component.html',
  styleUrls: ['./registro-empleado.component.sass']
})
export class RegistroEmpleadoComponent {
  areas: string[] = [
    'Administración',
    'Financiera',
    'Compras',
    'Infraestructura',
    'Operación',
    'Talento Humano',
    'Servicios Varios'
    // Agrega más áreas según sea necesario
  ];

  nuevoEmpleado: Empleado = {
    id: 0,
    primer_apellido: '',
    segundo_apellido: '',
    primer_nombre: '',
    otros_nombres: '',
    pais_empleo: '',
    tipo_identificacion: '',
    numero_identificacion: '',
    correo_electronico: '',
    fecha_ingreso: new Date(),
    area: '',
    estado : 'Activo',
    fecha_registro: new Date()
  };
  errorRegistro: string | null = null;  // Asegúrate de tener esta línea
  mensajeRegistro: string | null = null;  // Asegúrate de tener esta línea
  datosEmpleado: any;  // Puedes cambiar 'any' al tipo correcto de tu objeto Empleado


  ngOnInit() {
    // Inicializar el estado como "Activo"
    this.establecerFechaIngreso();
    this.nuevoEmpleado.estado = 'Activo';
  }

  constructor(private empleadoService: EmpleadoService,private snackBar: MatSnackBar) { }
  
  establecerFechaIngreso() {
    const fechaActual = new Date();
    const unMesAtras = new Date();
    unMesAtras.setMonth(unMesAtras.getMonth() - 1);

    // Verificar si la fecha de ingreso es mayor a la fecha actual
    if (this.nuevoEmpleado.fecha_ingreso > fechaActual) {
      // Establecer la fecha de ingreso como la fecha actual en formato UTC
      this.nuevoEmpleado.fecha_ingreso = fechaActual.toISOString().split('T')[0];
    }

    // Verificar si la fecha de ingreso es menor a un mes atrás
    if (this.nuevoEmpleado.fecha_ingreso < unMesAtras.toISOString().split('T')[0]) {
      // Establecer la fecha de ingreso como un mes atrás en formato UTC
      this.nuevoEmpleado.fecha_ingreso = unMesAtras.toISOString().split('T')[0];
    }
  }

  getMaxDate(): string {
    const today = new Date();
    const maxDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
    return maxDate.toISOString().split('T')[0];
  }

  getCurrentDateTime(): string {
    const now = new Date();
    return `${this.formatTwoDigits(now.getDate())}/${this.formatTwoDigits(now.getMonth() + 1)}/${now.getFullYear()} ${this.formatTwoDigits(now.getHours())}:${this.formatTwoDigits(now.getMinutes())}:${this.formatTwoDigits(now.getSeconds())}`;
  }

  formatTwoDigits(value: number): string {
    return value < 10 ? `0${value}` : `${value}`;
  }
  
  generarCorreoElectronico() {
    // Lógica para generar automáticamente el correo electrónico
    const primerNombre = this.nuevoEmpleado.primer_nombre?.toLowerCase() || '';
    const primerApellido = this.nuevoEmpleado.primer_apellido?.toLowerCase() || '';
    const dominio = this.nuevoEmpleado.pais_empleo === 'Colombia' ? 'cidenet.com.co' : 'cidenet.com.us';

    this.nuevoEmpleado.correo_electronico = `${primerNombre}.${primerApellido}@${dominio}`;
  }
  
  onInputChange() {
    this.generarCorreoElectronico();
  }
  

  registrarEmpleado() {
    console.log(this.nuevoEmpleado, 'this.nuevoEmpleado')
    this.empleadoService.registrarEmpleado(this.nuevoEmpleado).subscribe(
      response => {
        console.log('Empleado registrado exitosamente:', response);
        // Limpiar el formulario después del registro exitoso
        this.nuevoEmpleado = {
          id: 0,
          primer_apellido: '',
          segundo_apellido: '',
          primer_nombre: '',
          otros_nombres: '',
          pais_empleo: '',
          tipo_identificacion: '',
          numero_identificacion: '',
          correo_electronico: '',
          fecha_ingreso: new Date(),
          area: '',
          estado : 'Activo',
          fecha_registro: new Date()
        };

        this.mostrarMensaje('Empleado registrado exitosamente', 'success');

      },
      error => {
        console.error('Error al registrar empleado:', error);
        this.mostrarMensaje('Error al registrar empleado', 'error');
      }
    );
  }

  mostrarMensaje(mensaje: string, tipo: string) {
    // Implementa lógica para mostrar el mensaje al usuario, por ejemplo, usando una librería de alertas o un componente de notificación.
    // Por ejemplo, si estás utilizando Angular Material, puedes usar MatSnackBar para mostrar el mensaje.
    // Aquí hay un ejemplo básico:
    this.snackBar.open(mensaje, 'Cerrar', { duration: 5000, panelClass: tipo });
  }
}
