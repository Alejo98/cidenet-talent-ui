import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistroEmpleadoComponent } from './registro-empleado/registro-empleado.component';
import { ConsultaEmpleadosComponent } from './consulta-empleados/consulta-empleados.component';
import { EdicionEmpleadoComponent } from './edicion-empleado/edicion-empleado.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // Importa HttpClientModule
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';


@NgModule({
  declarations: [
    AppComponent,
    RegistroEmpleadoComponent,
    ConsultaEmpleadosComponent,
    EdicionEmpleadoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatOptionModule,
    MatFormFieldModule,
    MatSelectModule,
    FormsModule,
    MatInputModule,
    HttpClientModule,
    MatSnackBarModule,
    BrowserAnimationsModule, // Agrega HttpClientModule aquí

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
