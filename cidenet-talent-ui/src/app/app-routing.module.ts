import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultaEmpleadosComponent } from './consulta-empleados/consulta-empleados.component';
import { EdicionEmpleadoComponent } from './edicion-empleado/edicion-empleado.component';
import { RegistroEmpleadoComponent } from './registro-empleado/registro-empleado.component';

const routes: Routes = [
  { path: 'consulta-empleados', component: ConsultaEmpleadosComponent },
  { path: 'edicion-empleado/:id', component: EdicionEmpleadoComponent },
  { path: 'registro-empleado', component: RegistroEmpleadoComponent },
  { path: '', redirectTo: '/consulta-empleados', pathMatch: 'full' }, // Ruta por defecto
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
