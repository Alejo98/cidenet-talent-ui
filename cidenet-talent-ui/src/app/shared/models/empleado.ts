// empleado.model.ts

export interface Empleado {
    id: number;
    primer_apellido: string;
    segundo_apellido: string;
    primer_nombre: string;
    otros_nombres?: string;
    pais_empleo: string;
    tipo_identificacion: string;
    numero_identificacion: string;
    correo_electronico: string;
    fecha_ingreso: any;
    area: string;
    estado: string;
    fecha_registro: Date;
  }
  