// consulta-empleados.component.ts

import { Component, OnInit } from '@angular/core';
import { EmpleadoService } from '../empleado.service';
import { Empleado } from '../shared/models/empleado';
import { Router } from '@angular/router';

@Component({
  selector: 'app-consulta-empleados',
  templateUrl: './consulta-empleados.component.html',
  styleUrls: ['./consulta-empleados.component.sass']  // Cambia la extensión a .sass
})
export class ConsultaEmpleadosComponent implements OnInit {
  empleados: Empleado[] = [];
  filtroNombre: string = '';

  constructor(private empleadoService: EmpleadoService, private router: Router) { }

  ngOnInit() {
    this.obtenerEmpleados();
  }

  obtenerEmpleados() {
    this.empleadoService.consultarEmpleados().subscribe(
      response => {
        this.empleados = response;
      },
      error => {
        console.error('Error al obtener empleados:', error);
      }
    );
  }

  filtrarEmpleados() {
    if (this.filtroNombre) {
      this.empleadoService.filtrarEmpleados(this.filtroNombre).subscribe(
        response => {
          this.empleados = response;
        },
        error => {
          console.error('Error al filtrar empleados:', error);
        }
      );
    } else {
      // Si el filtro está vacío, obtener todos los empleados
      this.obtenerEmpleados();
    }
  }

  editarEmpleado(empleado: Empleado) {
    this.router.navigate(['/edicion-empleado', empleado.id]);
  }

  eliminarEmpleado(empleado: Empleado) {
    const confirmacion = confirm(`¿Está seguro de que desea eliminar a ${empleado.primer_nombre} ${empleado.primer_apellido}?`);

    if (confirmacion) {
      this.empleadoService.eliminarEmpleado(empleado.id).subscribe(
        response => {
          console.log('Empleado eliminado exitosamente:', response);
          // Actualizar la lista de empleados después de la eliminación
          this.obtenerEmpleados();
        },
        error => {
          console.error('Error al eliminar empleado:', error);
        }
      );
    }
  }
}
